<?php
// access to local json file
// thanks to Adriano https://stackoverflow.com/a/43597562/5168907
// global $our_domain;

require_once( "check_domain.php" );

if ( $details['site'] == "brighton" ) {
    $our_json = "js/brighton_list.json";
}


/*
$our_domain = $_SERVER['HTTP_HOST'];
  
if (
    $our_domain == ( "fbarton.alwaysdata.net" || "digitalbrightonandhove.org.uk" ) ) {
		$our_json = "js/dbh_assetsv2.json";
		$our_geojson = "js/dbh_assets.geojson";
	}
else if (
    $our_domain == ( "digitalgwynedd.wales" || "gwyneddddigidol.cymru" ) ) {
	      $our_json = "js/gwd_assets.json";
    }
else {
	echo "unexpected host domain";
    return;
}
*/

global $directory;
$directory = trailingslashit( get_template_directory_uri() );
  
$url = $directory . $our_json;
$geo_url = $directory . $our_geojson;

// Make the request
$request = wp_remote_get( $url );
if ( is_wp_error( $request ) ) {
    echo "Error finding ", $url;
	return false; // Bail early - if there's no data then might as well stop!
}
// else {
//  echo $url, " found successfully"; // uncomment to debug
// }

// Make the request
$geo_request = wp_remote_get( $geo_url );
if ( is_wp_error( $request ) ) {
    echo "Error finding ", $geo_url;
	return false; // Bail early - if there's no data then might as well stop!
}

// Retrieve the data
$json_body = wp_remote_retrieve_body( $request );
$geojson_body = wp_remote_retrieve_body( $geo_request );
// global $results;
$results = json_decode( $json_body, true );  // if problems, try removing 'true'
$geo_results = json_decode( $geojson_body, true );  // if problems, try removing 'true'
?>